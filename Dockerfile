FROM debian:10
RUN apt-get update && apt-get install -y build-essential cmake meson libwayland-dev wayland-protocols \
 libegl1-mesa-dev libgles2-mesa-dev libdrm-dev libgbm-dev libinput-dev \
 libxkbcommon-dev libudev-dev libpixman-1-dev libsystemd-dev libcap-dev \
 libxcb1-dev libxcb-composite0-dev libxcb-xfixes0-dev libxcb-xinput-dev \
 libxcb-image0-dev libxcb-render-util0-dev libx11-xcb-dev libxcb-icccm4-dev \
 freerdp2-dev libwinpr2-dev libpng-dev libavutil-dev libavcodec-dev \
 libavformat-dev universal-ctags git autoconf libtool libpcre3-dev \
 libcairo2-dev libpango1.0-dev libgdk-pixbuf2.0-dev xwayland curl xz-utils \
 libcanberra0 libxcb-xkb1
COPY install.sh /tmp/install.sh
RUN bash /tmp/install.sh
