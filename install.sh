#!/bin/bash

# Based on https://gist.github.com/davidrios/535c43cbaafe6be665b7c6bd345ea6f4, from
# https://github.com/swaywm/sway/wiki/Debian-10-(Buster)-Installation

set -e

function reconfig_meson() {
	if [ -d build ]; then
		rm -r build/meson-logs
		meson build --wipe
	else
		meson build
	fi
}

echo 'Enabling contrib and non-free repositories'
sed -i -e 's/main$/main contrib non-free/g' /etc/apt/sources.list
apt update

mkdir -p ~/sway-src

echo 'Installing wlroots...'

apt install -y build-essential cmake meson libwayland-dev wayland-protocols \
 libegl1-mesa-dev libgles2-mesa-dev libdrm-dev libgbm-dev libinput-dev \
 libxkbcommon-dev libudev-dev libpixman-1-dev libsystemd-dev libcap-dev \
 libxcb1-dev libxcb-composite0-dev libxcb-xfixes0-dev libxcb-xinput-dev \
 libxcb-image0-dev libxcb-render-util0-dev libx11-xcb-dev libxcb-icccm4-dev \
 freerdp2-dev libwinpr2-dev libpng-dev libavutil-dev libavcodec-dev \
 libavformat-dev universal-ctags git
cd ~/sway-src
[ ! -d wlroots ] && git clone https://github.com/swaywm/wlroots.git
cd wlroots
git fetch
git checkout 0.7.0
reconfig_meson
ninja -C build
ninja -C build install
ldconfig


echo 'Installing json-c...'

apt install -y autoconf libtool
cd ~/sway-src
[ ! -d json-c ] && git clone https://github.com/json-c/json-c.git
cd json-c
git fetch
git checkout json-c-0.13.1-20180305
sh autogen.sh
./configure --enable-threading --prefix=/usr/local
CPUCOUNT=$(grep processor /proc/cpuinfo | wc -l)
make -j$CPUCOUNT
make install
ldconfig


echo 'Installing scdoc'

cd ~/sway-src
[ ! -d scdoc ] && git clone https://git.sr.ht/~sircmpwn/scdoc
cd scdoc
git fetch
git checkout 1.9.7
make PREFIX=/usr/local -j$CPUCOUNT
make PREFIX=/usr/local install


echo 'Installing sway'

apt install -y libpcre3-dev libcairo2-dev libpango1.0-dev libgdk-pixbuf2.0-dev xwayland
cd ~/sway-src
[ ! -d sway ] && git clone https://github.com/swaywm/sway.git
cd sway
git fetch
git checkout 1.2
reconfig_meson
ninja -C build
ninja -C build install


echo 'Installing swaybg'

cd ~/sway-src
[ ! -d swaybg ] && git clone https://github.com/swaywm/swaybg.git
cd swaybg
git fetch
git checkout 1.0 
reconfig_meson
ninja -C build
ninja -C build install


apt install -y curl xz-utils libcanberra0 libxcb-xkb1
mkdir -p /opt/kitty
curl -L https://github.com/kovidgoyal/kitty/releases/download/v0.14.4/kitty-0.14.4-x86_64.txz | tar xvJ -C /opt/kitty
ln -sf /opt/kitty/bin/kitty /usr/local/bin
sed -i -e 's/urxvt/kitty/g' /usr/local/etc/sway/config


echo 'All set, now you should be able to just execute "sway" from a tty.'
echo 'The default key combination for opening a terminal in Sway is <WinKey>+<Enter>'
